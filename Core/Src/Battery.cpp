#include "Battery.h"

Battery::Battery(/* args */)
{
  info.cellCount = 4;
  info.currentMeasChnCount = 1;
  info.totalAdcMeasChnCount = MEAS_CHN_COUNT;
  
  info.maxCurrent     = 1667; // ~7A
  info.minVoltage     = 3200; // 3.6V
  info.critMinVoltage = 3150; // 3.5V
}

Battery::~Battery()
{
}

static void stupidWait()
{
  for (int i = 0; i < 100; ++i)
  {
    #ifndef TEST
    HAL_GetTick();
    #endif
  }
}

BatMeasRes& Battery::getBatMeas()
{
  #ifndef TEST

  HAL_ADC_Start_DMA(hadc1, (uint32_t*)measRes.adcResult, MEAS_CHN_COUNT);

  stupidWait();
  //HAL_Delay(ADC_TIMEOUT_MS);

  HAL_ADC_Stop_DMA(hadc1);

  #else

  for (int i = 0; i < MEAS_CHN_COUNT; i++)
  {
    measRes.adcResult[i] = i;
  }

  #endif

  return measRes;
}

BatteryState Battery::undervoltageCheck()
{
  static uint8_t undervoltageCounter = 0;
  static uint8_t critvoltageCounter = 0;
  uint8_t batMissingCounter = 0;

  for (uint8_t i = 1; i < info.totalAdcMeasChnCount; i++)
  { 

    if (measRes.adcResult[i] == 0)
    {
      batMissingCounter++;
      continue;
    }

    if (measRes.adcResult[i] < info.critMinVoltage)
    {
      critvoltageCounter++;
      break;
    }

    if (measRes.adcResult[i] < info.minVoltage)
    {
      undervoltageCounter++;
      break;
    }
  }

  if (batMissingCounter == info.cellCount)
  {
    return BATTERY_MISSING;
  }

  if (critvoltageCounter > 2)
  {
    return BATTERY_CRIT;
  }
  
  if (undervoltageCounter > 3)
  {
    return BATTERY_LOW;
  }

  return BATTERY_OK;
}

BatteryState Battery::overcurrentCheck()
{
  static int overcurrentCounter = 0;

  if (measRes.adcResult[0] > info.maxCurrent)
  {
    overcurrentCounter++;
  }
  
  if (overcurrentCounter > 1)
  {
    return BATTERY_OVERCURRENT;
  }

  return BATTERY_OK;
}

BatteryState Battery::getBatState()
{
  BatteryState status = overcurrentCheck();
  if (status != BATTERY_OK)
  {
    return status;
  }

  return undervoltageCheck();
}

BatInfo& Battery::getBatInfo()
{
  return info;
}
