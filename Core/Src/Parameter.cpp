#include "Parameter.h"
#include <cctype>
#include <cstdlib>

bool isNumber(const char* s, int len)
{
  int i = 0;
  while (i < len && std::isdigit(s[i])) 
  {
    ++i;
  }

  return i != 0 && i == len;
}

int ParameterInt::setValue(const char* input, int len)
{
  if (!isNumber(input, len))
  {
    return -1;
  }

  int tmp = strtol(input, NULL, 10);

  return setRawValue(tmp);
}

int ParameterInt::setRawValue(int input)
{
  if (input < minValue || input > maxValue)
  {
    return -1;
  }

  value = input;

  return 0;
}

int ParameterString::setValue(const char* input, int len)
{
  if (value)
  {
    delete[] value;
  }

  value = new char[len];
  strncpy(value, input, len);
  length = len;

  return 0;
}