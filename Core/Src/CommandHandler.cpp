#include <cstring>
#include "CommandHandler.h"
#include "Command.h"
#include "Defines.h"
#include "Battery.h"

#define COMMANDS_COUNT 5

static Command* commandRegister[COMMANDS_COUNT];

static Battery* battery;

static const char* ErrorMessages[] = {"OK", 
                                      "Unknown command", 
                                      "Wrong parameter value", 
                                      "Too many parameters",
                                      "Too few parameters",
                                      "Undefined"};

static int generateErrorMsg(ErrorCodes code, char* msg)
{
  int offset = 0;
  offset += sprintf(msg + offset, "ERROR ");
  offset += sprintf(msg + offset, "%s\n", ErrorMessages[(int)code]);
  return offset;
}

#ifdef __cplusplus
extern "C" {
#endif


int initCommandHandler()
{
  commandRegister[0] = (new Command_lsbat);
  commandRegister[1] = (new Command_lschn);
  commandRegister[2] = (new Command_setchn);
  commandRegister[3] = (new Command_reset);
  commandRegister[4] = (new Command_setpin);

  battery = new Battery();

  ((Command_lsbat*)commandRegister[0])->bindBattery(battery);

  return 0;
}

void setAdcHandle(ADC_HandleTypeDef* adc)
{
  battery->setAdc(adc);

  return;
}

void clearCommandHandler()
{
  for (int i = 0; i < COMMANDS_COUNT; i++)
  {
    if (commandRegister[i])
    {
      delete commandRegister[i];
      commandRegister[i] = nullptr;
    }
  }

  if (battery)
  {
    delete battery;
    battery = nullptr;
  }
  
  return;
}

int processCommand(const char* cmd, char* output, const int outBuffSize)
{
  unsigned int orderLen = 0;

  if (!cmd || !output)
  {
    return (int)ErrorCodes::ERROR_UNDEFINED;
  }

  for (int j = 0; j < RECEIVE_BUFFER_SIZE; j++)
  {
    if (isSpaceChar(cmd[j]) || isTerminatingChar(cmd[j]))
    {
      orderLen = j;
      break;
    }
  }

  for (int i = 0; i < COMMANDS_COUNT; i++)
  {
    if (strncmp(cmd, commandRegister[i]->getName(), orderLen) == 0 && 
          strlen(commandRegister[i]->getName()) == orderLen)
    {
      commandRegister[i]->reset();
      commandRegister[i]->init();

      ErrorCodes status = commandRegister[i]->parseParams(cmd);

      if (status != ErrorCodes::ERROR_NONE)
      {
        generateErrorMsg(status, output);
        return (int)status;
      }

      status = commandRegister[i]->process();

      if (status != ErrorCodes::ERROR_NONE)
      {
        generateErrorMsg(status, output);
        return (int)status;
      }

      status = commandRegister[i]->getOutput(output, outBuffSize);

      if (status != ErrorCodes::ERROR_NONE)
      {
        generateErrorMsg(ErrorCodes::ERROR_UNDEFINED, output);
        return (int)ErrorCodes::ERROR_UNDEFINED;
      }

      return (int)ErrorCodes::ERROR_NONE;
    }
  }

  generateErrorMsg(ErrorCodes::ERROR_COMMAND_UNKNOWN, output);
  return (int)ErrorCodes::ERROR_COMMAND_UNKNOWN;
}

BatteryState getBatteryStatus()
{
  battery->getBatMeas();
  return battery->getBatState();
}

void setPowerSectionsState(unsigned int newState)
{
  return setChannelState(newState);
}

unsigned int getPowerSectionsState()
{
  return getChannelState();
}

#ifdef __cplusplus
}
#endif
