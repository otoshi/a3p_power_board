/*
 * Command.cpp
 *
 *  Created on: Sep 22, 2020
 *      Author: Tomasz Gos
 */

#include "Command.h"
#include "Battery.h"

static unsigned int channelState = 0;

static const unsigned int switchesGpios[] = { SWITCH_3V3_Pin,
                                              SWITCH_5V_Pin, 
                                              SWITCH_12V_Pin, 
                                              SWITCH_V_MOT_Pin };

static const unsigned int switchesMasks[] = { SWITCH_3V3_MASK,
                                              SWITCH_5V_MASK, 
                                              SWITCH_12V_MASK, 
                                              SWITCH_VMOT_MASK };

static const unsigned int userGpioPins[] =  { USER_GPIO_0_Pin,
                                              USER_GPIO_1_Pin,
                                              USER_GPIO_2_Pin,
                                              USER_GPIO_3_Pin,
                                              USER_GPIO_4_Pin,
                                              USER_GPIO_5_Pin,
                                              USER_GPIO_6_Pin,
                                              USER_GPIO_7_Pin };
 
static GPIO_TypeDef* userGpioPorts[] = { USER_GPIO_0_GPIO_Port,
                                         USER_GPIO_1_GPIO_Port,
                                         USER_GPIO_2_GPIO_Port,
                                         USER_GPIO_3_GPIO_Port,
                                         USER_GPIO_4_GPIO_Port,
                                         USER_GPIO_5_GPIO_Port,
                                         USER_GPIO_6_GPIO_Port,
                                         USER_GPIO_7_GPIO_Port };


bool isSpaceChar(char c)
{
  return c == ' ';
}

bool isTerminatingChar(char c)
{
  return c == '\0' || c == '\n' || c == '\r';
}

unsigned int getChannelState()
{
  return channelState;
}

void setChannelState(unsigned int newState)
{
  channelState = newState;

  #ifndef TEST

  for (uint8_t i = 0; i < 4; ++i)
  {
    if (channelState & ((uint8_t)(switchesMasks[i])))
    {
      HAL_GPIO_WritePin(SWITCH_GPIO_PORT, switchesGpios[i], GPIO_PIN_SET);
    }
    else
    {
      HAL_GPIO_WritePin(SWITCH_GPIO_PORT, switchesGpios[i], GPIO_PIN_RESET);
    }
  }

  #endif
}

ErrorCodes Command::parseParams(const char* cmd)
{
  const char* args[RECEIVE_BUFFER_SIZE];
  int argsLen[RECEIVE_BUFFER_SIZE];

  int argIter = 0;
  int prevPos = 0;

  int lastSymbol = 0;

  for (int j = strlen(cmd) - 1; j >= 0; j--)
  {
    if (!isSpaceChar(cmd[j]) && !isTerminatingChar(cmd[j]))
    {
      lastSymbol = j;
      break;
    }
  }

  for (int j = 0; j < RECEIVE_BUFFER_SIZE; j++)
  {
    if (isSpaceChar(cmd[j]) && j < lastSymbol)
    {
      args[argIter] = &(cmd[j+1]);
      
      if (argIter != 0)
      {
        argsLen[argIter-1] = j - prevPos - 1;
      }
      prevPos = j;
      argIter++;
      
    }
    else if (isSpaceChar(cmd[j]) || isTerminatingChar(cmd[j]))
    {
      args[argIter] = &(cmd[j+1]);
      if (argIter != 0)
      {
        argsLen[argIter-1] = j - prevPos - 1;
      }

      break;
    }
  }

  if (argIter > paramsInSize)
  {
    return ErrorCodes::ERROR_TOO_MANY_PARAMS;
  }
  else if (argIter < paramsInSize)
  {
    return ErrorCodes::ERROR_TOO_FEW_PARAMS;
  }

  argIter = 0;

  for (int j = 0; j < paramsInSize; j++)
  {
    if (paramsIn[j]->setValue(args[argIter], argsLen[argIter]) != 0)
    {
      return ErrorCodes::ERROR_WRONG_PARAMETER_VALUE;
    }
    argIter++;
  }
  
  return ErrorCodes::ERROR_NONE;
}

void Command::dumpCommand(char* msg)
{
  int offset = 0;

  offset += sprintf(msg+offset, "%s takes %i input arguments: ", name, paramsInSize);
  
  for (int j = 0; j < paramsInSize; j++)
  {
    offset += sprintf(msg+offset, "%s(%s) ", paramsIn[j]->getName(), paramsIn[j]->getType());
  }
  
  offset += sprintf(msg+offset, "and returns %i output arguments: ", paramsOutSize);
  
  for (int j = 0; j < paramsOutSize; j++)
  {
    offset += sprintf(msg+offset, "%s(%s) ", paramsOut[j]->getName(), paramsOut[j]->getType());
  }

  offset += sprintf(msg+offset, "\n");

  return;

}

ErrorCodes Command::getOutput(char* outMsg, int bufferMaxSize)
{
  int offset = 0;
  char argVal[MAX_ARG_LEN] = {0};

  offset += sprintf(outMsg + offset, "OK");

  for (int i = 0; i < paramsOutSize; i++)
  {
    if (offset < bufferMaxSize)
    {
      int written = paramsOut[i]->getValue(argVal);

      if (written < 0)
      {
        return ErrorCodes::ERROR_UNDEFINED;
      }

      sprintf(outMsg + offset, " %s", argVal);
      offset += written + 1; 
    }
    else
    {
      return ErrorCodes::ERROR_UNDEFINED;
    } 
  }

  offset += sprintf(outMsg + offset, "\n");

  return ErrorCodes::ERROR_NONE;
}

ErrorCodes Command_lsbat::process()
{
  if(!battery)
  {
    return ErrorCodes::ERROR_UNDEFINED;
  }

  BatMeasRes& measRes = battery->getBatMeas();

  if (battery->getBatInfo().totalAdcMeasChnCount != paramsOutSize - 1)
  {
    return ErrorCodes::ERROR_UNDEFINED;
  }

  BatteryState status = battery->getBatState();
  const char* statusMsg = "";

  switch (status)
    {
      case BATTERY_OVERCURRENT:
      {
        statusMsg = "WARN! OVERCURRENT!";
        break;
      }  
      case BATTERY_MISSING:
      {
        statusMsg = "WARN! NO BATTERY DETECTED!";
        break;
      }
      case BATTERY_CRIT:
      {
        statusMsg = "WARN! BATTERY CRITICALLY LOW!";
        break;
      }
      case BATTERY_LOW:
      {
        statusMsg = "WARN! BATTERY LOW!";
        break;
      }    
      default:
        statusMsg = "BATTERY GOOD"; 
        break;
    }

  //
  // Set measured values
  //

  for (int i = 0; i < paramsOutSize - 1; i++)
  {
    ((ParameterInt*)paramsOut[i])->setRawValue((int)measRes.adcResult[i]);
  }

  //
  // Set status
  //

  paramsOut[paramsOutSize - 1] ->setValue(statusMsg, strlen(statusMsg));

  return ErrorCodes::ERROR_NONE;
}


ErrorCodes Command_lschn::process()
{
  // TODO: Check for possibilities to enable rtti to use dynamic_cast here

  ParameterInt* param = static_cast<ParameterInt*>(paramsOut[0]);

  if (param)
  {
    param->setRawValue(channelState);

    return ErrorCodes::ERROR_NONE;
  }

  return ErrorCodes::ERROR_UNDEFINED;
}

ErrorCodes Command_setchn::process()
{
  // TODO: Check for possibilities to enable rtti to use dynamic_cast here

  ParameterInt* param = static_cast<ParameterInt*>(paramsIn[0]);

  setChannelState(param->getRawValue());

  ParameterInt* paramOut = static_cast<ParameterInt*>(paramsOut[0]);
  paramOut->setRawValue(getChannelState());

  return ErrorCodes::ERROR_NONE;
}

ErrorCodes Command_reset::process()
{
  #ifndef TEST
  HAL_NVIC_SystemReset();
  #endif

  return ErrorCodes::ERROR_NONE;
}

ErrorCodes Command_setpin::process()
{
  #ifndef TEST

  int pin = ((ParameterInt*)paramsIn[0])->getRawValue();
  GPIO_PinState state = (GPIO_PinState)((ParameterInt*)paramsIn[1])->getRawValue();

  HAL_GPIO_WritePin(userGpioPorts[pin], userGpioPins[pin], state);
    
  ((ParameterInt*)paramsOut[0])->setRawValue((int)pin);
  ((ParameterInt*)paramsOut[1])->setRawValue((int)state);

  #endif

  return ErrorCodes::ERROR_NONE;
}

#ifdef TEST

ErrorCodes Command_example::process()
{
  //
  // 1. Load input param values to local variables
  //

  ParameterString* paramStr = (ParameterString*)paramsIn[0];
  ParameterInt* param = (ParameterInt*)paramsIn[1];

  char someString[RECEIVE_BUFFER_SIZE];

  paramStr->getValue(someString);
  int someUInt = param->getRawValue();

  //
  // 2. Do some fancy processing
  //

  someString[0] += 23;
  someString[1] = 'D';
  someString[2] = '\0';
  
  someUInt += 123;

  //
  // 3. Assign local variables values to output params
  //

  ParameterString* paramOutStr = (ParameterString*)paramsOut[0];
  ParameterInt* paramOut = (ParameterInt*)paramsOut[1];

  paramOutStr->setValue(someString, strlen(someString));
  paramOut->setRawValue(someUInt);

  return ErrorCodes::ERROR_NONE;
}

#endif
