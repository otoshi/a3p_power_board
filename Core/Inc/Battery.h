/*
 * Parameter.h
 *
 *  Created on: Sep 30, 2020
 *      Author: Tomasz Gos
 */

#ifndef INC_BATTERY_H_
#define INC_BATTERY_H_

#ifdef TEST
typedef unsigned int uint32_t;
typedef unsigned char uint8_t;
typedef int ADC_HandleTypeDef;

#else
#include "main.h"
#endif

#include "Defines.h"

class Battery
{

public:
  Battery(/* args */);
  ~Battery();

  void setAdc(ADC_HandleTypeDef* hadc)
  {
    hadc1 = hadc;
  }

  BatteryState getBatState();
  BatMeasRes&  getBatMeas();
  BatInfo&     getBatInfo();

protected:

  ADC_HandleTypeDef* hadc1;
  BatMeasRes measRes;
  BatInfo info;
  BatteryState batStatus;

  BatteryState undervoltageCheck();
  BatteryState overcurrentCheck();

};

#endif /* INC_BATTERY_H_ */
