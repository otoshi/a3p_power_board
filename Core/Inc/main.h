/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f3xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define I_MEAS_Pin GPIO_PIN_0
#define I_MEAS_GPIO_Port GPIOA
#define CELL_1_ADC_Pin GPIO_PIN_1
#define CELL_1_ADC_GPIO_Port GPIOA
#define CELL_0_ADC_Pin GPIO_PIN_2
#define CELL_0_ADC_GPIO_Port GPIOA
#define CELL_2_ADC_Pin GPIO_PIN_3
#define CELL_2_ADC_GPIO_Port GPIOA
#define CELL_3_ADC_Pin GPIO_PIN_4
#define CELL_3_ADC_GPIO_Port GPIOA
#define USER_GPIO_0_Pin GPIO_PIN_5
#define USER_GPIO_0_GPIO_Port GPIOA
#define USER_GPIO_1_Pin GPIO_PIN_6
#define USER_GPIO_1_GPIO_Port GPIOA
#define USER_GPIO_2_Pin GPIO_PIN_7
#define USER_GPIO_2_GPIO_Port GPIOA
#define USER_GPIO_3_Pin GPIO_PIN_0
#define USER_GPIO_3_GPIO_Port GPIOB
#define USER_GPIO_4_Pin GPIO_PIN_1
#define USER_GPIO_4_GPIO_Port GPIOB
#define USER_GPIO_5_Pin GPIO_PIN_8
#define USER_GPIO_5_GPIO_Port GPIOA
#define USER_GPIO_6_Pin GPIO_PIN_11
#define USER_GPIO_6_GPIO_Port GPIOA
#define USER_GPIO_7_Pin GPIO_PIN_12
#define USER_GPIO_7_GPIO_Port GPIOA
#define SWITCH_3V3_Pin GPIO_PIN_4
#define SWITCH_3V3_GPIO_Port GPIOB
#define SWITCH_5V_Pin GPIO_PIN_5
#define SWITCH_5V_GPIO_Port GPIOB
#define SWITCH_12V_Pin GPIO_PIN_6
#define SWITCH_12V_GPIO_Port GPIOB
#define SWITCH_V_MOT_Pin GPIO_PIN_7
#define SWITCH_V_MOT_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
#define RECEIVE_BUFFER_SIZE 16
#define RESPONSE_BUFFER_SIZE 64
#define UART_TIMEOUT_MS 50
#define ADC_TIMEOUT_MS 10
#define UART_TIMEOUT_MS 50
#define STATUS_SUCCESS 0

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
