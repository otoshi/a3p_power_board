#ifndef INC_COMMANDHANDLER_H_
#define INC_COMMANDHANDLER_H_

#include "Defines.h"

#ifndef TEST
#include "main.h"
#else
typedef int ADC_HandleTypeDef;
#endif

#ifdef __cplusplus
extern "C" {
#endif

int initCommandHandler();
void clearCommandHandler();
void setAdcHandle(ADC_HandleTypeDef* adc);
int processCommand(const char* cmd, char* output, const int outBuffSize);
BatteryState getBatteryStatus();
void setPowerSectionsState(unsigned int newState);
unsigned int getPowerSectionsState();


#ifdef __cplusplus
}
#endif

#endif
