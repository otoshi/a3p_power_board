/*
 * Command.h
 *
 *  Created on: Sep 22, 2020
 *      Author: Tomasz Gos
 */

#ifndef INC_COMMAND_HL_
#define INC_COMMAND_HL_

#include "Parameter.h"
#include "Battery.h"

#ifndef TEST
#include "main.h"
#else
typedef unsigned int GPIO_TypeDef;
typedef int ADC_HandleTypeDef;
#endif

bool isSpaceChar(char c);

bool isTerminatingChar(char c);

void setChannelState(unsigned int newState);

unsigned int getChannelState();

class Command
{
  public:

  Command() = delete;

  virtual ~Command()
  {
    for (int i = 0; i < paramsInSize; i++)
    {
      delete paramsIn[i];
    }

    for (int i = 0; i < paramsOutSize; i++)
    {
      delete paramsOut[i];
    }

    if (paramsIn)
    {
      delete[] paramsIn;
      paramsIn = nullptr;
    }

    if (paramsOut)
    {
      delete[] paramsOut;
      paramsOut = nullptr;
    }

  }

  const char* getName()
  {
    return name;
  }

  virtual void reset()
  {

  }
  
  virtual void init()
  {

  }

  virtual ErrorCodes getOutput(char* outMsg, int size);
  
  virtual ErrorCodes parseParams(const char* s);

  void dumpCommand(char* msg);

  virtual ErrorCodes process() = 0;

  protected:

  Command(const char* name) : name(name)
  {}

  const char* name;

  Parameter** paramsIn = nullptr;
  Parameter** paramsOut = nullptr;

  int paramsInSize = 0;
  int paramsOutSize = 0;
};

class Command_lsbat : public Command
{
  public:

  Command_lsbat() : Command("lsbat")
  {
    paramsInSize = 0;
    paramsOutSize = 6;

    paramsOut = new Parameter*[paramsOutSize];

    paramsOut[4] = new ParameterInt("I_MEAS", 0, 4096);
    paramsOut[0] = new ParameterInt("CELL_0", 0, 4096);
    paramsOut[1] = new ParameterInt("CELL_1", 0, 4096);
    paramsOut[2] = new ParameterInt("CELL_2", 0, 4096);
    paramsOut[3] = new ParameterInt("CELL_3", 0, 4096);
    paramsOut[5] = new ParameterString("Status");
  }

  virtual ~Command_lsbat(){}

  virtual ErrorCodes process();

  void bindBattery(Battery* bat)
  {
    battery = bat;
  }

  protected:

  Battery* battery;
};

class Command_lschn : public Command
{
  public:

  Command_lschn() : Command("lschn")
  {
    paramsInSize = 0;
    paramsOutSize = 1;

    paramsOut = new Parameter*[paramsOutSize];

    paramsOut[0] = new ParameterInt("channelState", 0, 15);
    
  }

  virtual ~Command_lschn(){}

  virtual ErrorCodes process();
};

class Command_setchn : public Command
{
  public:

  Command_setchn() : Command("setchn")
  {
    paramsInSize = 1;
    paramsOutSize = 1;

    paramsIn = new Parameter*[paramsInSize];
    paramsOut = new Parameter*[paramsOutSize];

    paramsIn[0] = new ParameterInt("channelStateIn", 0, 15);
    paramsOut[0] = new ParameterInt("channelStateOut", 0, 15);
  }

  virtual ~Command_setchn(){}

  virtual ErrorCodes process();
};

class Command_reset : public Command
{
  public:

  Command_reset() : Command("reset")
  {
    paramsInSize = 0;
    paramsOutSize = 0;
  }

  virtual ~Command_reset(){}

  virtual ErrorCodes process();
};

class Command_setpin : public Command
{
  public:

  Command_setpin() : Command("setpin")
  {
    paramsInSize = 2;
    paramsOutSize = 2;

    paramsIn = new Parameter*[paramsInSize];
    paramsOut = new Parameter*[paramsOutSize];

    paramsIn[0] = new ParameterInt("pin", 0, 7);
    paramsIn[1] = new ParameterInt("state", 0, 1);

    paramsOut[0] = new ParameterInt("pin", 0, 7);
    paramsOut[1] = new ParameterInt("state", 0, 1);
  }

  virtual ~Command_setpin(){}

  virtual ErrorCodes process();
};
#ifdef TEST

class Command_example : public Command
{
  public:

  Command_example() : Command("example")
  {
    paramsInSize = 2;
    paramsOutSize = 2;

    paramsIn = new Parameter*[paramsInSize];
    paramsOut = new Parameter*[paramsOutSize];

    paramsIn[0] = new ParameterString("a");
    paramsIn[1] = new ParameterInt("b", 0, 15);

    paramsOut[0] = new ParameterString("c");
    paramsOut[1] = new ParameterInt("d", 0, 300);
  }

  virtual ~Command_example(){}

  virtual ErrorCodes process();
};

#endif /* TEST */


#endif /* INC_COMMANDHANDLER_HL_ */
