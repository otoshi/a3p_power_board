/*
 * Parameter.h
 *
 *  Created on: Sep 25, 2020
 *      Author: Tomasz Gos
 */

#ifndef INC_PARAMETER_H_
#define INC_PARAMETER_H_

#include <cstring>
#include <stdio.h>

#include "Defines.h"
#include "ErrorCodes.h"

bool isNumber(const char* s);

class Parameter
{
  public:

  Parameter() = delete;

  virtual ~Parameter(){}

  const char* getName()
  {
    return name;
  }

  const char* getType()
  {
    return type;
  }

  virtual int setValue(const char* input, int len) = 0;

  virtual int getValue(char* output) = 0;

  protected:

  Parameter(const char* name, const char* type) : name(name), type(type)
  {}

  const char* name;
  const char* type;

};

class ParameterInt : public Parameter
{
  public:

  ParameterInt(const char* name, unsigned int min, unsigned int max) : 
  Parameter(name, "int"), minValue(min), maxValue(max)
  {}

  ParameterInt() = delete;

  virtual ~ParameterInt(){}

  virtual int setValue(const char* input, int len);

  virtual int setRawValue(int input);

  virtual int getValue(char* output)
  {
    return sprintf(output, "%u", value);
  }

  int getRawValue()
  {
    return value;
  }

  protected:

  int value;
  int minValue;
  int maxValue;

};

class ParameterString : public Parameter
{
  public:

  ParameterString(const char* name) : Parameter(name, "string")
  {}

  ParameterString() = delete;

  virtual ~ParameterString()
  {
    if (value)
    {
      delete[] value;
      value = nullptr;
    }
  }

  virtual int setValue(const char* input, int len);

  virtual int getValue(char* output)
  {
    if (value)
    {
      strncpy(output, value, length);
      return length;
    }
    
    return -1;
  };

  protected:

  char* value = nullptr;
  int length = 0;

};



#endif /* INC_PARAMETER_H_ */
