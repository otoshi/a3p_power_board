#ifndef INC_DEFINES_H_
#define INC_DEFINES_H_

#define MAX_ARG_LEN 30

#define SWITCH_3V3_MASK  0x01
#define SWITCH_5V_MASK   0x02
#define SWITCH_12V_MASK  0x04
#define SWITCH_VMOT_MASK 0x08

#ifndef TEST

#include "main.h"

//
// Power section switches
//

#define SWITCH_GPIO_PORT GPIOB

//
// Stepper controls
//

#define STEPPER_GPIO_PORT GPIOA
#define STEPPER_STEP_PIN  GPIO_PIN_11
#define STEPPER_DIR_PIN	  GPIO_PIN_12

#else

#define RECEIVE_BUFFER_SIZE 16
#define SWITCH_GPIO_PORT 0
#define SWITCH_3V3_Pin   0
#define SWITCH_5V_Pin    0 
#define SWITCH_12V_Pin   0 
#define SWITCH_V_MOT_Pin 0

#define STEPPER_GPIO_PORT 0
#define STEPPER_STEP_PIN  0
#define STEPPER_DIR_PIN	  0

#define USER_GPIO_0_Pin 0
#define USER_GPIO_1_Pin 0
#define USER_GPIO_2_Pin 0
#define USER_GPIO_3_Pin 0
#define USER_GPIO_4_Pin 0
#define USER_GPIO_5_Pin 0
#define USER_GPIO_6_Pin 0
#define USER_GPIO_7_Pin 0

#define USER_GPIO_0_GPIO_Port 0
#define USER_GPIO_1_GPIO_Port 0
#define USER_GPIO_2_GPIO_Port 0
#define USER_GPIO_3_GPIO_Port 0
#define USER_GPIO_4_GPIO_Port 0
#define USER_GPIO_5_GPIO_Port 0
#define USER_GPIO_6_GPIO_Port 0
#define USER_GPIO_7_GPIO_Port 0 

typedef unsigned int uint32_t;
typedef unsigned char uint8_t;
typedef int ADC_HandleTypeDef;

#endif

//
// Battery related
//

#define MEAS_CHN_COUNT 5

typedef enum _BatteryState
{
  BATTERY_OK,
  BATTERY_LOW,
  BATTERY_CRIT,
  BATTERY_MISSING,
  BATTERY_OVERCURRENT,
}BatteryState;

typedef struct _BatMeasRes
{
  volatile uint32_t adcResult[MEAS_CHN_COUNT];
} BatMeasRes;

typedef struct _BatInfo
{
  uint8_t cellCount;
  uint8_t currentMeasChnCount;
  uint8_t totalAdcMeasChnCount;

  uint32_t maxCurrent;
  uint32_t minVoltage;
  uint32_t critMinVoltage;
} BatInfo;

#endif
