#include <iostream>
#include "CommandHandler.h"
#include "gtest/gtest.h"

typedef int ADC_HandleTypeDef;

int outBufLen = 80;

TEST (lsBat, lsBatTest)
{
  initCommandHandler();

  char outBuff[outBufLen];

  ASSERT_EQ(0, (int)processCommand("lsbat", outBuff, outBufLen));

  ASSERT_EQ(0, strcmp(outBuff, "OK 0 1 2 3 4 BATTERY GOOD\n"));

  ASSERT_EQ(3, (int)processCommand("lsbat 4", outBuff, outBufLen));
  ASSERT_EQ(3, (int)processCommand("lsbat hwpd leoeoe", outBuff, outBufLen));

  ASSERT_EQ(1, (int)processCommand("lsasdsad", outBuff, outBufLen));
  ASSERT_EQ(1, (int)processCommand("sa fdsokf sdf", outBuff, outBufLen));
  ASSERT_EQ(1, (int)processCommand("", outBuff, outBufLen));

  clearCommandHandler();
}

TEST (ChannelState, lsChnTest)
{
  initCommandHandler();

  char outBuff[outBufLen];

  ASSERT_EQ(0, (int)processCommand("lschn", outBuff, outBufLen));

  ASSERT_EQ(0, strcmp(outBuff, "OK 0\n"));

  ASSERT_EQ(3, (int)processCommand("lschn hwpd leoeoe", outBuff, outBufLen));

  ASSERT_EQ(1, (int)processCommand("lsasdsad", outBuff, outBufLen));
  ASSERT_EQ(1, (int)processCommand("lschnfdsokf", outBuff, outBufLen));
  ASSERT_EQ(1, (int)processCommand("", outBuff, outBufLen));
  
  clearCommandHandler();
}


TEST (ChannelState, setChnTest)
{
  initCommandHandler();

  char outBuff[outBufLen];

  char cmp[15];
  char cmd[15];

  for (int i = 0; i < 16; i++)
  {
    sprintf(cmd, "setchn %i", i);
    sprintf(cmp, "OK %i\n", i);
    ASSERT_EQ(0, (int)processCommand(cmd, outBuff, outBufLen));
    ASSERT_EQ(0, (int)processCommand("lschn", outBuff, outBufLen));
    ASSERT_EQ(0, strcmp(outBuff, cmp));
  }

  ASSERT_EQ(4, (int)processCommand("setchn", outBuff, outBufLen));
  ASSERT_EQ(3, (int)processCommand("setchn 2 1", outBuff, outBufLen));

  ASSERT_EQ(2, (int)processCommand("setchn -1", outBuff, outBufLen));
  ASSERT_EQ(2, (int)processCommand("setchn 16", outBuff, outBufLen));

  ASSERT_EQ(1, (int)processCommand("setchn1 1", outBuff, outBufLen));
  ASSERT_EQ(1, (int)processCommand("sa fdsokf sdf", outBuff, outBufLen));
  ASSERT_EQ(1, (int)processCommand("", outBuff, outBufLen));

  clearCommandHandler();
}

TEST (reset, resetTest)
{
  initCommandHandler();

  char outBuff[outBufLen];

  ASSERT_EQ(0, (int)processCommand("reset", outBuff, outBufLen));

  ASSERT_EQ(0, strcmp(outBuff, "OK\n"));

  ASSERT_EQ(3, (int)processCommand("reset 4", outBuff, outBufLen));
  ASSERT_EQ(3, (int)processCommand("reset hwpd leoeoe", outBuff, outBufLen));

  ASSERT_EQ(1, (int)processCommand("resetsetet", outBuff, outBufLen));
  ASSERT_EQ(1, (int)processCommand("sa fdsokf sdf", outBuff, outBufLen));
  ASSERT_EQ(1, (int)processCommand("", outBuff, outBufLen));

  clearCommandHandler();
}

int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  
  return RUN_ALL_TESTS();
}