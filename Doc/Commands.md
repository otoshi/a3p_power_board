# User Guide

## Table of contents
  - [Commands format](#commands-format)
  - [Commands list](#commands-list)
    - [Error handling](#error-handling)
  - [Asynchronous messages](#asynchronous-messages)
  - [UART configuration](#uart-communication-info)

## Commands format: 

`command` PARAM1 [min, max] "PARAM2" ...

`response` PARAM3 [min, max] PARAM4 ...

If parameter is followed by square brackets [x,y] it's type is int. Numbers inside brackets describe minimum and maximum possible value of the parameter. 
If parameter is in quotation marks it's a string parameter.

## Commands list:

1. `lsbat`
   
      `OK` I_MEAS* CELL_0* CELL_1* CELL_2* CELL_3* "STATUS"
        
      *[0, 4096]

      **Description:** Get battery info: battery voltage and current flowing through vbat output. The overcurrent and undervoltage check is done aswell.

      **Out parameters:** 
      - I_MEAS - current measured on VBAT output. 1 unit = 4.2 mA. Continuous current greater than 1667 (7A) can damage the board.
      - CELL_N - voltage on Nth cell (min: 0 = 0V, max 4096 = 5V). In practice the voltage on single cell of 4S lipo should never exceed 4.2V, so 3750 in measurement ouput.
      - STATUS - overcurrent and undervoltage check result:
        -  `WARN! OVERCURRENT!` - current flowing through vBat output exceeds 7A
        -  `WARN! BATTERY LOW!` - one of the lipo cells below 3.6V
        -  `WARN! BATTERY CRITICALLY LOW!` - one of the lipo cells below 3.5V
        -  `WARN! NO BATTERY DETECTED!` - balancer output not plugged in
        -  `BATTERY GOOD`

      **Examples**: 
      
      `lsbat`

      `OK 72 3445 3420 3442 3426 BATTERY GOOD`

      ~300 mA load, 3.85V per cell = 14.90V (~40% charged)

      `lsbat`

      `OK 0 3170 3172 3169 3171 WARN! BATTERY LOW!`

      0 mA load, 3,56V per cell = 14.24V (0% charged)

      `lsbat`

      `OK 36 0 0 0 0 WARN! NO BATTERY DETECTED!`

      150 mA load, 0V per cell (no battery)

2. `lschn`

    `OK` CHANNEL_STATE [0, 15]

      **Description:** Get state of voltage section switches.

      **Out parameters:** 
      - CHANNEL_STATE - States are written on 4 lowest bits of the response.

        - BIT_0 - 3v3 switch,
        - BIT_1 - 5v switch,
        - BIT_2 - 12v switch,
        - BIT_3 - vbat switch,

      **Example**: 
      
      `lschn`

      `OK 5`  (0000 0000 0000 0101 - 3v3 and 12v ON, 5v and vbat OFF)

3. *`setchn`* CHANNEL_STATE [0, 15]

    `OK` CHANNEL_STATE [0, 15]

      **Description:** Set state of voltage section switches

      **In parameters:** 
      - CHANNEL_STATE - as above

      **Out parameters:** 
      - CHANNEL_STATE - as above

      **Example**:
      
     `setchn 7`

      `OK 7` 

4. `setpin` PIN [0, 7] STATE [0, 1]

    `OK` PIN [0, 15] STATE [0, 1] 

      **Description:** Set user GPIO state.

      [//]: # (TODO: Add GPIOs image here)

      **In parameters:** 
      - PIN - user GPIO pin number
      - STATE - 0-LOW, 1-HIGH

      **Out parameters:** 
      - PIN - as above
      - STATE - as above

      **Example**:
      
     `setpin 7 1`

      `OK 7 1` 

5. `reset`
   
    `OK`

      **Description:** Set board to initial state
    
## Error handling

If command execution fails, the proper error message will be returned instead of a regular message.
        
 Error messages:
  - `ERROR Unknown command`,
  - `ERROR Wrong parameter value` - wrong parameter type (letters in Int parameter) or Int parameter is out of range,
  - `ERROR Too many parameters`,
  - `ERROR Too few parameters`,
  - `ERROR Undefined` - Should not happen in normal usage. Used for implementation errors handling (null pointer occurence etc.),

# Asynchronous messages
If undervoltage of the battery or overcurrent on the vBat power section occurs, the board will inform the user by sending an asynchronous UART message. These messages are consistent with `lsbat` STATUS messages with `ASYNC` prefix. Additionally, the following actions will be performed to prevent hardware damage.

-  `ASYNC WARN! OVERCURRENT!` - vBat section shutdown,
-  `ASYNC WARN! BATTERY LOW!` - vBat, 12V section shutdown,
-  `ASYNC WARN! BATTERY CRITICALLY LOW!` - vBat, 12V, 5V, 3V3 section shutdown,
-  `ASYNC WARN! NO BATTERY DETECTED!` -  no action

# UART communication info:
The communication with the board is based on a fixed-length UART messages.

RX UART callback is triggered every 16 bytes, so the command needs to be 16 bytes long. Input can be terminated with `\n` `\r` or `\0`.

The response is 64 bytes long and is terminated with `\n`.

**UART config:**
- Baud rate = 115200
- Data bits = 8
- Stop Bits = 1
- Parity = None
- Flow control = None