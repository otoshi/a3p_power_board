# a3p_power_board

This repository contains driver code for power management board used in a3p robot project (https://gitlab.com/otoshi/a3p). The board was designed for this particular project, however it's quite universal and can be used in any mechatronic project.

## Table of contents
  - [Main features](#main-features)
  - [Software usage](#software-usage)
  - [Development notes](#development-notes)
  - [Adding a new command](#adding-a-new-command)
  - [License](#license)
  - [Contact info](#contact-info)
  - [User Guide](./Doc/Commands.md)

## Main features:

1. 4S LiPo battery cell voltage measurement with discharge protection.
2. Current measurement for Vbat output line.
3. 4 switchable power sections: 
   - Vbat - 6 outputs,
   - 12V  - 2 outputs,
   - 5V   - 4 outputs,
   - 3V3  - 4 outputs.
4. 5V USB-A output compatible with Raspberry Pi power supply requirements.
5. 8 user GPIOs.
6. Communication via UART.

### Board overview:

![PCB image](Doc/a3p_pmb.png)

## Software usage

The board is controlled by a set of commands sent via UART. The current implementation contains following commands:

1. `lsbat`              - get battery info (battery voltage and motor current)
2. `lschn`              - get state of voltage section switches
3. `setchn`             - set state of voltage section switches
4. `setpin`             - set user GPIO state
5. `reset`              - set board to initial state

For detailed info see [User Guide](./Doc/Commands.md).

## Development notes
The project is written in STM32CubeIDE, so the compilation and uploading via ST-Link should be straightforward.
All commands work in blocking mode. Non-blocking mode for long-executable commands is the main TBD target.

## Adding a new command

If you wish to add a new command to the existing ones here's the simple recipe. It's best to base your new command on the existing ones to keep consistency.

1. Create new subclass of Command in [Command.h](./Core/Inc/Command.h).
2. Add input and output parameters (Int and String parameter types are supported in the current version).
3. Implement process() method for your new command subclass.
4. Add your new command to commandRegister in [CommandHandler.cpp](./Core/Src/CommandHandler.cpp).
5. Increase COMMANDS_COUNT constant in [CommandHandler.cpp](./Core/Src/CommandHandler.cpp).

## License
This software is distributed under BSD 3-Clause License.

## Contact info

gomektos@gmail.com